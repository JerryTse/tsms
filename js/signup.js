var signup = angular.module('signup', []);

signup.controller('signupController', function ($scope, $http, $location, $sessionStorage) {
    $scope.checkExist = function () {
        check().then(function (error, response, body) {
            return false
        }).catch(function (error) {
            return true
        });
    };

    var check = function () {
        return new Promise((resolve, reject) => {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", url + "users/check/" + $scope.username, true);
            xhr.send();

            if (xhr.status == 200) {
                resolve();
            }
            if (xhr.status == 409) {
                reject();
            }
        });
    };

    $scope.registerforseeker = function () {
        if ($scope.checkExist()) {
            alert("User Name already exist.")
        } else {
            var username = $scope.username;
            var password = $scope.password;
            var englishname = $scope.englishname;
            var chinesename = $scope.chinesename;
            var mobileno = $scope.mobileno;
            var contactno = $scope.contactno;
            var email = $scope.email;
            var residencedistrict = $scope.residencedistrict;
            var residenceaddress = $scope.residenceaddress;
            var relationshipwithstudent = $scope.relationshipwithstudent;
            
            var req = {
                method: 'POST',
                url: url + 'users',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    "userType": "Student",
                    "username": username,
                    "englishName": englishname,
                    "chineseName": chinesename,
                    "password": password,
                    "mobileNo": mobileno,
                    "contactNo": contactno,
                    "email": email,
                    "residenceDistrict": residencedistrict,
                    "residenceAddress": residenceaddress
                }
            }
            $http(req).then(function successCallback(response) {
                response.data;
                if (response.status == 201) {
                    $scope.message = "Register Succeed";
                    $location.path("/message/" + $scope.message);
                }
                if (response.status == 400) {
                    $scope.message = response.data.message
                }
            }, function errorCallback(response) {
                $scope.message = response.data.message
            });
        }
    }

    $scope.registerfortutor = function () {
        if ($scope.checkExist()) {
            alert("User Name already exist.")
        } else {
            var username = $scope.username;
            var password = $scope.password;
            var englishname = $scope.englishname;
            var chinesename = $scope.chinesename;
            var mobileno = $scope.mobileno;
            var contactno = $scope.contactno;
            var email = $scope.email;
            var residencedistrict = $scope.residencedistrict;
            var residenceaddress = $scope.residenceaddress;
            var gender = $scope.gender;
            var dateofbirth = $scope.dateofbirth;
            var cat = {
                categories: $scope.categories,
                levels: $scope.levels,
                subjects: $scope.subjects
            };
            var tutorCategory = [];
            tutorCategory.push(cat);

            var req = {
                method: 'POST',
                url: url + 'users',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    "userType": "Tutor",
                    "username": username,
                    "englishName": englishname,
                    "chineseName": chinesename,
                    "password": password,
                    "mobileNo": mobileno,
                    "contactNo": contactno,
                    "email": email,
                    "residenceDistrict": residencedistrict,
                    "gender": gender,
                    "dateOfBirth": dateofbirth,
                    "tutorCategory": tutorCategory
                }
            }

            $http(req).then(function successCallback(response) {
                response.data;
                if (response.status == 201) {
                    $scope.message = "Register Succeed";
                    $location.path("/message/" + $scope.message);
                }
                if (response.status == 400) {
                    $scope.message = response.data.message
                }
            }, function errorCallback(response) {
                $scope.message = response.data.message
            });
        }
    }
});