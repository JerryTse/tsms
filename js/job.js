var job = angular.module('job', []);

job.controller('jobController', function ($scope, $http, $location, $sessionStorage, $rootScope, $routeParams) {
    var init = function () {
        if ($routeParams.jobid === undefined) {
            $scope.contactnumber = $sessionStorage.currentUser.contactNo;
            $scope.hour = 1;
            $scope.minute = 0;
        } else {
            var req = {
                method: 'GET',
                url: url + 'jobs/job/' + $routeParams.jobid,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            $http(req).then(function successCallback(response) {
                response.data;
                if (response.status != 200) {
                    $scope.message = response.data.message
                } else {
                    $scope.job = response.data;
                    $scope.subject = $scope.job.subject;
                    $scope.numberofstudents = $scope.job.numberOfStudents;
                    $scope.location = $scope.job.location;
                    $scope.tuitionfeeperUnit = $scope.job.tuitionFeePerUnit;
                    $scope.tuitionunit = $scope.job.tuitionUnit;
                    $scope.hour = $scope.job.lessonDuration.hour;
                    $scope.minute = $scope.job.lessonDuration.minute;
                    $scope.lessontime = $scope.job.lessonTime;
                    $scope.contactnumber = $scope.job.contactNumber;
                    $scope.remark = $scope.job.remark;

                    $scope.exist = false;

                    for (var i = 0; i < $sessionStorage.favoritelist.length; i++) {
                        if ($sessionStorage.favoritelist[i]["_id"] === $scope.job._id) {
                            $scope.exist = true;
                        }
                    }
                }
            }, function errorCallback(response) {
                $scope.message = response.data.message
            });
        }
    }
    init();

    $scope.createjob = function () {
        var authorizationBasic = btoa($sessionStorage.currentUser.username + ':' + $sessionStorage.currentUser.password);
        
        var district = $scope.district;
        var location = $scope.location;
        var lat = $scope.lat;
        var lng = $scope.lng;
        var subject = $scope.subject;
        var tuitionfeeperUnit = $scope.tuitionfeeperUnit;
        var tuitionunit = $scope.tuitionunit;
        var numberofstudents = $scope.numberofstudents;
        var tutoracademicrequirements = $scope.tutoracademicrequirements;
        var tutorgenderrequest = $scope.tutorgenderrequest;
        var numberoftimesperWeek = $scope.numberoftimesperWeek;
        var lessonduration = {
            "hour": $scope.hour,
            "minute": $scope.minute
        };
        var lessontime = $scope.lessontime;
        var createddatetime = $scope.createddatetime;
        var contactnumber = $scope.contactnumber;
        var remark = $scope.remark;
        var status = $scope.status;

        if ($scope.hour < 0 || $scope.minute < 0) {
            $scope.message = "Lesson Duration can not be less then 0."
            return;
        }

        var req = {
            method: 'POST',
            url: url + 'jobs',
            headers: {
                'Authorization': 'Basic ' + authorizationBasic,
                'Content-Type': 'application/json'
            },
            data: {
                "district": district,
                "location": location,
                "lat": lat,
                "lng": lng,
                "subject": subject,
                "tuitionFeePerUnit": tuitionfeeperUnit,
                "tuitionUnit": tuitionunit,
                "numberOfStudents": numberofstudents,
                "tutorAcademicRequirements": tutoracademicrequirements,
                "tutorGenderRequest": tutorgenderrequest,
                "numberOfTimesPerWeek": numberoftimesperWeek,
                "lessonDuration": lessonduration,
                "lessonTime": lessontime,
                "createdDateTime": Date.now(),
                "contactNumber": contactnumber,
                "remark": remark,
                "relatedStudentUser": $sessionStorage.currentUser._id
            }
        }
        $http(req).then(function successCallback(response) {
            response.data;
            if (response.status == 201) {
                $scope.message = "Job Created";
                $location.path("/message/" + $scope.message);
            }
            if (response.status == 400) {
                $scope.message = response.data.message
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    };

    $scope.acceptjob = function () {
        var authorizationBasic = btoa($sessionStorage.currentUser.username + ':' + $sessionStorage.currentUser.password);

        var req = {
            method: 'PUT',
            url: url + 'jobs/' + $routeParams.jobid,
            headers: {
                'Authorization': 'Basic ' + authorizationBasic,
                'Content-Type': 'application/json'
            },
            data: {
                "relatedTutorUser": $sessionStorage.currentUser._id
            }
        }
        $http(req).then(function successCallback(response) {
            response.data;
            if (response.status == 200) {
                $scope.message = "You have accepted a job.";
                $location.path("/message/" + $scope.message);
            } else {
                $scope.message = response.data.message
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    };

    $scope.addtofavorite = function () {
        if ($scope.exist == false) {
            if ($sessionStorage.favoritelist === undefined || $sessionStorage.favoritelist == null) {
                $sessionStorage.favoritelist = [];
            }
            $sessionStorage.favoritelist.push($scope.job);
            $scope.exist = true;
        }
    }
});