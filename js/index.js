/*global angular  */
var url = "http://billfyp.myds.me:8083/";

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var app = angular.module('app', ['ngRoute', 'ngStorage', 'signup', 'loginlogout', 'job', 'myjoblist', 'myfavoritelist']);

/* the config function takes an array. */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/signup', {
            templateUrl: 'templates/signup.html'
        })
        .when('/signupforseeker', {
            templateUrl: 'templates/signupforseeker.html',
            controller: 'signupController'
        })
        .when('/signupfortutor', {
            templateUrl: 'templates/signupfortutor.html',
            controller: 'signupController'
        })
        .when('/message/:message', {
            templateUrl: 'templates/message.html',
            controller: 'messageController'
        })
        .when('/login', {
            templateUrl: 'templates/login.html',
            controller: 'loginlogoutController'
        })
        .when('/logout', {
            templateUrl: 'templates/logout.html',
            controller: 'loginlogoutController'
        })
        .when('/createjob', {
            templateUrl: 'templates/createjob.html',
            controller: 'jobController'
        })
        .when('/updatejob/:jobid', {
            templateUrl: 'templates/updatejob.html',
            controller: 'updatejobController'
        })
        .when('/viewjobdetail/:jobid', {
            templateUrl: 'templates/viewandacceptjob.html',
            controller: 'jobController'
        })
        .when('/myjoblist', {
            templateUrl: 'templates/myjoblist.html',
            controller: 'myjoblistController'
        })
        .when('/myfavoritelist', {
            templateUrl: 'templates/myfavoritelist.html',
            controller: 'myfavoritelistController'
        })
        
        .otherwise({
            redirectTo: '/main',
            templateUrl: 'templates/main.html',
            controller: 'mainController'
        })
}])

app.controller('messageController', function ($routeParams, $scope) {
    $scope.message = $routeParams.message;
});

app.controller('mainController', function ($scope, $http, $location, $sessionStorage, $rootScope) {
    $scope.searchText = "";

    $scope.init = function () {
        var req = {
            method: 'POST',
            url: url + 'joblist',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                "subject": $scope.searchText
            }
        }
        $http(req).then(function successCallback(response) {
            response.data;
            if (response.status != 200) {
                $scope.message = response.data.message
            } else {
                $scope.jobs = response.data;
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    };
    $scope.init();
});