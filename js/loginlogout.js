var loginlogout = angular.module('loginlogout', []);

loginlogout.controller('loginlogoutController', function ($scope, $http, $location, $sessionStorage, $rootScope) {
    $scope.login = function () {
        var authorizationBasic = btoa($scope.username + ':' + $scope.password);

        var req = {
            method: 'POST',
            url: url + 'login',
            headers: {
                'Authorization': 'Basic ' + authorizationBasic,
                'Content-Type': 'application/json'
            }
        }
        $http(req).then(function successCallback(response) {
            response.data;
            if (response.status == 400 || response.status == 404) {
                $scope.message = response.data.message
            } else {
                $rootScope.currentUser = response.data;
                $sessionStorage.currentUser = response.data;
                $location.path("/");
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    };

    $scope.logout = function () {
        $rootScope.currentUser = undefined;
        $sessionStorage.currentUser = undefined;
        $location.path("/");
    };
});