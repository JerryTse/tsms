var myjoblist = angular.module('myjoblist', []);

myjoblist.controller('myjoblistController', function ($scope, $http, $location, $sessionStorage, $rootScope, $routeParams) {
    $scope.init = function () {
        var joburl = "";

        if ($sessionStorage.currentUser.userType == "Tutor") {
            joburl = "jobs/tutor/" + $sessionStorage.currentUser._id
        } else {
            joburl = "jobs/student/" + $sessionStorage.currentUser._id
        }

        var authorizationBasic = btoa($sessionStorage.currentUser.username + ':' + $sessionStorage.currentUser.password);

        var req = {
            method: 'GET',
            url: url + joburl,
            headers: {
                'Authorization': 'Basic ' + authorizationBasic,
                'Content-Type': 'application/json'
            }
        }
        $http(req).then(function successCallback(response) {
            response.data;
            if (response.status != 200) {
                $scope.message = response.data.message
            } else {
                $scope.jobs = response.data;
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    };
    $scope.init();

    $scope.delete = function (jobid) {
        var authorizationBasic = btoa($sessionStorage.currentUser.username + ':' + $sessionStorage.currentUser.password);

        var req = {
            method: 'DELETE',
            url: url + "jobs/" + jobid,
            headers: {
                'Authorization': 'Basic ' + authorizationBasic,
                'Content-Type': 'application/json'
            }
        }
        $http(req).then(function successCallback(response) {
            if (response.status != 204) {
                $scope.message = response.data.message
                $location.path("/message/" + $scope.message);
            } else {
                $scope.init();
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    }

    $scope.cancel = function (jobid) {
        var authorizationBasic = btoa($sessionStorage.currentUser.username + ':' + $sessionStorage.currentUser.password);

        var req = {
            method: 'PUT',
            url: url + "jobs/" + jobid,
            headers: {
                'Authorization': 'Basic ' + authorizationBasic,
                'Content-Type': 'application/json'
            },
            data: {
                "relatedTutorUser": null
            }
        }
        $http(req).then(function successCallback(response) {
            if (response.status != 200) {
                $scope.message = response.data.message
                $location.path("/message/" + $scope.message);
            } else {
                $scope.init();
            }
        }, function errorCallback(response) {
            $scope.message = response.data.message
        });
    }
});