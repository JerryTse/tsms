var myfavoritelist = angular.module('myfavoritelist', []);

myfavoritelist.controller('myfavoritelistController', function ($scope, $http, $location, $sessionStorage, $rootScope, $routeParams) {
    var init = function () {
        $scope.jobs = $sessionStorage.favoritelist;
    };
    init();

    $scope.delete = function (jobid) {

        for (var i = 0; i < $sessionStorage.favoritelist.length; i++) {
            if ($sessionStorage.favoritelist[i]["_id"] == jobid) {
                $sessionStorage.favoritelist.splice(i, 1);
            }
        }

        $location.path("/myfavoritelist");
    };
});